.PHONY: pdf
pdf:
	pandoc --output resume.pdf resume.md

.PHONY: viewpdf
viewpdf:
	type xdg-open >/dev/null 2>&1 && xdg-open resume.pdf || open resume.pdf
