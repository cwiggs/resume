---
title: Chris Wiggins
geometry: 
  - margin=12mm
---

# Contact
(480).389.5659 | me[at]cwiggs[.]com | [cwiggs.com](cwiggs.com)

## Skills

---

### Infrastructure/Platform Engineering
- Containers: Kubernetes, ECS (Fargate and EC2), Docker, Compose,  Podman.
- AWS: EC2, EFS, EBS, RDS, VPC, IAM, SQS, SNS, Kinesis, Cloudwatch, etc.
- Azure: Vnets, VMs, SQL Server, HDinsights, Databricks, etc.
- Linux: AWS linux, CentOS/RHEL, Ubuntu/Debian, Alpine.

### DevOps / SRE
- Monitoring: Prometheus, Netdata, Nagios, Opentelemetry, Dynatrace.
- CI/CD: Gitlab CI, Circleci, Jenkins (shared library development).
- Logging: Sumologic, Splunk, Fluentd.

### Programming
- Java/Groovy with gradle for testing.
- Python, Golang, and javascript(nodejs) for low level languages.
- Terraform (using terratest), json, yaml, toml for declarative languages.
- Advanced level of git (rebase vs squash, cherry-pick, fetch vs pull, etc). 
- Extensive use of Gnu Make to automate common tasks.

### Miscellaneous
- Atlassian tools (Jira, Confluence, Bamboo, etc)
- Scrum, Kanban, Agile development.

## Experience

---

### Arcade | Infrastructure Engineer | March 2022 - Current
- Broke down current Terraform monorepo into smaller repos to control deployments easier.
- Added tests to current Terraform codebase using Terratest.
- Built various CircleCI CI/CD pipelines to deploy Terraform code.
- Implemented argoCD "app of apps" method to deploy kubernetes deployments with gitops.
- Helped developers write and deploy container infrastructure.

### NortonLifelock | Senior Site Reliability Engineer | January 2017 - March 2022
- Responsible for creating a new Kubernetes environment, including Jenkins CI/CD pipelines.
- Implement new and improved existing ECS deployment.
- Main developer in charge of SDLC around Jenkins shared libraries, using gradle for unit tests.
- Main developer in charge of creating reusable Terraform modules for the rest of the company.
- Responsible for creating various Jenkins CICD pipelines, kubectl, dockerBuild, etc.
- Continued to build all infrastructure using Terraform, and bootstrap legacy systems with Chef.
- Migrated all infrastructure from COLO to AWS with Terraform/Chef on time.

### Newtek Technology Solutions | Systems Engineer | March 2016 - January 2017
- Work with customers to setup and maintain their Windows or Linux environments.
- Work with customer to setup and maintain their Sophos UTM/XG and virtual Sonicwall firewall/routers.  
- Deploy Windows and Linux Shared environments, such as installing and configuring Cpanel, CSF/iptables, Apache, Ansible, IIS, SmarterMail, MySQL/MSSQL.  
- Maintain Windows and Linux Shared environments, for example: keep mail spool low on Exim and SmarterMail, remove compromised accounts/files from mail, and/or cpanel accounts, as well as fix various software issues.

### Hyperion Works LLC. | Linux Specialist, Systems Administrator | September 2013 - March 2016
- Responsible for multiple Ubuntu/Debian domain controllers (Samba 3/4) and file servers (CIFS, NFS, SSHFS) with MDADM RAID arrays and LVM. 
- Deploy and maintain networks (DHCP, DNS, QoS, OpenWRT, OpenVPN, etc). 
- Provided on-site support, remote (VNC, RDP) support, and maintenance (software updates and security patches).
- Responsible for off-site and on-site backups using Rsnapshot and Acronis. 
- Configure and maintain VOIP systems (FreePBX, Asterisk), as well as the soft/hard phones.

## Homelab

---

I have a lab at home that I use for testing new technology and self-hosting.  A homelab has been extremely valuable
over the years to extend my experience.  I also love to hear what other engineers have setup at home.

- 3 HP Elitedesk g3 minis in a proxmox cluster.
- 1 HP elitedesk g3 mini running Truenas Core; acts as a NFS share for large data.
- k3s cluster with 3 nodes using etcd for a true HA setup.  Two k3s worker nodes.
